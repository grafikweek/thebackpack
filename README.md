# The Pannier

Home Exercises for students of Digital Design, Photoshop & Illustrator brought to you by [Weeklify](http://weeklify.com) 

## What's this?

**The Pannier** is a collection of free exercises designed to let you practice your techincal skills in Photoshop and Illustrator. Try to solve the puzzles and problem illustrated in each file

## How to use

1. Download the repo as ZIP *(or fork it if you'd like to create your own branch and commit changes)*
2. Unarchive the ZIP file
3. the exercise name shows you the topic and software: `software_#_topic_#_title`
4. Choose the software you want to improve: `p` for Photoshop and `i` for Illustrator
5. Choose the topic and complete the exercises in order
6. Enjoy :)

## Updates

We'll regularly update this repo with new exercises and improve the excisting, so come back and check it every now and then for new juicy challenges

## License

We release all the exercises under a Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license. Please spread the world but don't kill our work :)

## About the author

![alt text](https://gitlab.com/grafikweek/thepannier/raw/01bfcda79eed08beb463c727b78e82697816bdfe/Made%20by%20Weeklify%20Logo.jpg "Weeklify Logo")

Weeklify is an avant garde workshops hub with years of experience in creating engaging lecturers, experimental classes and role-flipping courses where the students are the true protagonists. Check out our weekly classes in London ([Grafik Bites](http://weeklify.com/grafikbites)) or our all time favorite 5 day long workshop [Grafik Week](http://weeklify.com/grafikweek) 